<?php

use yii\db\Migration;

/**
 * Handles the creation of table `breakdown`.
 */
class m170719_063802_create_breakdown_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('breakdown', [
            'id' => $this-> primaryKey(),
            'title'=> $this-> string(),
            'levelId'=> $this-> Integer(),
            'statusId' => $this->Integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('breakdown');
    }
}
