<?php

use yii\db\Migration;

/**
 * Handles the creation of table `level`.
 */
class m170719_084415_create_level_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
      $this->createTable('level', [
            'id' => $this-> primaryKey(),
            'name'=> $this-> string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('level');
    }
}
