<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "level".
 *
 * @property integer $id
 * @property string $name
 */
class Level extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'level';
    }
public static function getLevels()
    {
        $allLevels = self::find()->all();
        $allLevelsArray = ArrayHelper::
                    map($allLevels, 'id', 'name');
        return $allLevelsArray; 
    }
    /**
     * @inheritdoc
     */

    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }
}
