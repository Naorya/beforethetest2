<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "breakdown".
 *
 * @property integer $id
 * @property string $title
 * @property integer $levelId
 * @property integer $statusId
 */
class Breakdown extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'breakdown';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['levelId', 'statusId'], 'integer'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'levelId' => 'Level ID',
            'statusId' => 'Status ID',
        ];
    }
    public function getFindStatus()
    {
        return  Status::findOne($this->statusId);
    }
public function getFindLevel()
    {
        return Level::findOne($this->levelId);
    }
}
