<?php
namespace app\rbac;
use app\models\User;
use yii\rbac\Rule;
use Yii; 
use yii\db\ActiveRecord;

class OwnUpdateuserRule extends Rule
{
	public $name = 'OwnUpdateuserRule';

	public function execute($user, $item, $params)
	{
		if (!Yii::$app->user->isGuest) {
			if(isset($_GET['id'])){
			        
			    if(($_GET['id'] == $user))
			        return true;
			}
		}
		return false;
	}
}