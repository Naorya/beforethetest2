<?php
namespace app\rbac;
use app\models\User;
use yii\rbac\Rule;
use Yii; 
use yii\db\ActiveRecord;

class OwnViewurgentRule extends Rule
{
	public $name = 'OwnViewurgentRule';
//manger can view only
	public function execute($user, $item, $params)
	{	
			if(isset($user)){
				$currentUserRole = \Yii::$app->authManager->getRolesByUser($user);
				
				if($currentUserRole == 'manager')
					return false;
					
			}
		
			return false;
		}
	}
